/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include "pwm_impl.h"

mp_obj_t mp_pwm_init(mp_obj_t port)
{
    return mp_obj_new_int(IoTPwmInit(mp_obj_get_int(port))); 
}

mp_obj_t mp_pwm_deinit(mp_obj_t port)
{
    return mp_obj_new_int(IoTPwmDeinit(mp_obj_get_int(port))); 
}

mp_obj_t mp_pwm_start(mp_obj_t port, mp_obj_t duty, mp_obj_t freq)
{
    return mp_obj_new_int(IoTPwmStart(mp_obj_get_int(port), mp_obj_get_int(duty), mp_obj_get_int(freq))); 
}

mp_obj_t mp_pwm_stop(mp_obj_t port)
{
    return mp_obj_new_int(IoTPwmStop(mp_obj_get_int(port))); 
}


