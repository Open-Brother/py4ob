/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include "os_impl.h"

STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_os_sleep_obj, mp_os_sleep);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_os_usleep_obj, mp_os_usleep);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_os_delay_obj, mp_os_delay);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_os_delay_until_obj, mp_os_delay_until);
STATIC const MP_DEFINE_CONST_FUN_OBJ_0(mp_os_get_tick_obj, mp_os_get_tick);

STATIC const mp_rom_map_elem_t mp_module_os_globals_table[] = 
{
    {MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_os)},
    {MP_ROM_QSTR(MP_QSTR_sleep), MP_ROM_PTR(&mp_os_sleep_obj)},
    {MP_ROM_QSTR(MP_QSTR_usleep), MP_ROM_PTR(&mp_os_usleep_obj)},
    {MP_ROM_QSTR(MP_QSTR_delay), MP_ROM_PTR(&mp_os_delay_obj)},
    {MP_ROM_QSTR(MP_QSTR_delay_until), MP_ROM_PTR(&mp_os_delay_until_obj)},
    {MP_ROM_QSTR(MP_QSTR_get_tick), MP_ROM_PTR(&mp_os_get_tick_obj)},
};

STATIC MP_DEFINE_CONST_DICT(mp_module_os_globals, mp_module_os_globals_table); 

const mp_obj_module_t mp_module_os =
{
    .base = {&mp_type_module},    
    .globals = (mp_obj_dict_t*)&mp_module_os_globals,
};

MP_REGISTER_MODULE(MP_QSTR_os, mp_module_os, 1);
