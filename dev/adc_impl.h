/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#ifndef ADC_IMPL_H
#define ADC_IMPL_H

#include "py/objtype.h"
#include "py/runtime.h"

mp_obj_t mp_adc_set_bais(mp_obj_t index, mp_obj_t bais);
mp_obj_t mp_adc_set_model(mp_obj_t index, mp_obj_t model);
mp_obj_t mp_adc_set_reset_count(mp_obj_t index, mp_obj_t count);
mp_obj_t mp_adc_read(mp_obj_t index);
mp_obj_t mp_adc_query_bais_value(mp_obj_t key);
mp_obj_t mp_adc_query_model_value(mp_obj_t key);

#endif
