/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

#ifndef APP_UTILS_H
#define APP_UTILS_H

char* GetMode(void);
int SetMode(const char* mode);
int GetWifiInfo(char** id, char** pwd);
int SetWifiInfo(const char* id, const char* pwd);
void LastError(const char* err);
void ClearError(void);
char* GetError(void);

#endif