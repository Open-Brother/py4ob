/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
#include <malloc.h>
#include <string.h>
#include "ipc_message.h"

Message* Message_New(short type, short cmd, short index, short total, const char* payload, unsigned int length)
{
    Message* ret = malloc(sizeof(Message) + length);
    
    if( ret )
    {
        ret->type = type;
        ret->cmd = cmd;
        ret->index = index;
        ret->total = total;
        ret->length = length;
        
        if( payload )
        {
            memcpy(ret + 1, payload, length);
        }
    }
    
    return ret;
}

