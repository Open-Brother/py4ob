/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
#include "wifi_connect.h"
#include "config_context.h"
#include "app_utils.h"
#include "cmsis_os2.h"
#include "utils_file.h"
#include "hi_at.h"

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#define BEGIN_TAG  "<PY4OH>"
#define END_TAG    "<PY4OH>\v"

typedef struct
{
    const char* cmd;
    int(*handler)(const char**, size_t);
} CfgHander;

unsigned int Py_Cmd_Query(int argc, const char** argv)
{
    char* mode = GetMode();
    char* id = NULL;
    char* pwd = NULL;
    char* ip = Wifi_IpAddr();
    char* err = GetError();

    printf(BEGIN_TAG);

    if( mode )
    {
        printf("Mode: %s\n", mode);
    }
    else
    {
        printf("Mode: repl\n");
    }

    if( GetWifiInfo(&id, &pwd) == 0 )
    {
        printf("Wifi: %s\n", id);
        printf("Pwd: %s\n", pwd);
    }

    if( ip )
    {
        printf("IP: %s\n", ip);
    }

    if( err )
    {
        printf("Error: %s\n", err);
    }

    printf(END_TAG);

    (void)argc;
    (void)argv;

    return 0;
}

static int Mode_Handler(const char**argv, size_t argc)
{
    int ret = 0;

    if( argc == 1 )
    {
        ret = SetMode(argv[0]);
    }
    else
    {
        ret = -1;
    }

    return ret;
}

static int Wifi_Handler(const char**argv, size_t argc)
{
    int ret = 0;

    if( argc == 2 )
    {
        ret = SetWifiInfo(argv[0], argv[1]);
    }
    else
    {
        ret = -1;
    }

    return ret;
}

unsigned int Py_Cmd_Config(int argc, const char** argv)
{
    static const CfgHander s_Handler[] = 
    {
        {"mode", Mode_Handler},
        {"wifi", Wifi_Handler},
    };

    size_t i = 0;

    for(i=0; i<sizeof(s_Handler)/sizeof(*s_Handler); i++)
    {
        if( strcmp(argv[0], s_Handler[i].cmd) == 0 )
        {
            int err = s_Handler[i].handler(&argv[1], argc - 1);
            
            printf("%sConfig is %s!\n%s", BEGIN_TAG, !err ? "OK" : "failed", END_TAG);

            return 0;
        }
    }

    printf("%sInvalid Command: %s\n%s", BEGIN_TAG, argv[0], END_TAG);

    return 0;
}

static void* Config_Context_Task(const char* arg)
{
    static const at_cmd_func s_PyCmdTable[] = 
    {
        {"+QUERY", 6, 0, 0, 0, Py_Cmd_Query},
        {"+CONFIG", 7, 0, 0, Py_Cmd_Config, 0},
    };

    osDelay(100);

    ClearError();

    hi_at_register_cmd(s_PyCmdTable, sizeof(s_PyCmdTable)/sizeof(*s_PyCmdTable));
    
    return (void*)arg;
}

void Config_Context_Entry(void)
{
    osThreadAttr_t attr = {0};

    attr.name = "Config Task";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 1024 * 5;
    attr.priority = osPriorityNormal;

    if (osThreadNew((osThreadFunc_t)Config_Context_Task, NULL, &attr) == NULL)
    {
        printf("[dt4sw] Falied to create Config task!\n");
    }
}