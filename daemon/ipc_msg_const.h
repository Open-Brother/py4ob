/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
#ifndef IPC_MSG_CONST_H
#define IPC_MSG_CONST_H

enum
{
    IPC_TYPE_MSG = 0x00,
    IPC_TYPE_CMD = 0x01,
    IPC_TYPE_STATE = 0x02,

    IPC_MSG_OUTPUT = 0x10,

    IPC_CMD_CODE = 0x20,
    IPC_CMD_CODE_ACK = 0x21,
    IPC_CMD_RUN = 0x22,
    IPC_CMD_RUN_ACK = 0x23,
    IPC_CMD_FILE = 0x24,
    IPC_CMD_FILE_ACK = 0x25,

    IPC_STATE_CONN = 0x30,
    IPC_STATE_DSCN = 0x31,
    IPC_STATE_HOST = 0x32,
    IPC_STATE_SIGNAL = 0x33
};

#endif