/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include "py/compile.h"
#include "py/runtime.h"
#include "py/repl.h"
#include "py/gc.h"
#include "py/mperrno.h"
#include "py/objstr.h"

static char* g_stack_top = NULL;
static char g_heap[1024 * 25] = {0};

static void do_str(const char* src, mp_parse_input_kind_t input_kind)
{
    nlr_buf_t nlr = {0};
    
    if( nlr_push(&nlr) == 0 ) 
    {
        mp_lexer_t* lex = mp_lexer_new_from_str_len(MP_QSTR__lt_stdin_gt_, src, strlen(src), 0);
        qstr source_name = lex->source_name;        
        mp_parse_tree_t parse_tree = mp_parse(lex, input_kind);
        mp_obj_t module_fun = mp_compile(&parse_tree, source_name, true);
        
        mp_call_function_0(module_fun);
        
        m_free(lex);
        
        nlr_pop();
    } 
    else
    {
        mp_obj_print_exception(&mp_plat_print, (mp_obj_t)nlr.ret_val);
    }
}

static void do_file(const char* filename)
{
    nlr_buf_t nlr = {0};
    
    if( nlr_push(&nlr) == 0 )
    {
        mp_lexer_t* lex = mp_lexer_new_from_file(filename);
        qstr source_name = lex->source_name;
        mp_parse_tree_t parse_tree = mp_parse(lex, MP_PARSE_FILE_INPUT);
        mp_obj_t module_fun = mp_compile(&parse_tree, source_name, false);
        
        mp_call_function_0(module_fun);
        
        m_free(lex);
        
        nlr_pop();
    }
    else
    {
        mp_obj_print_exception(&mp_plat_print, (mp_obj_t)nlr.ret_val);
    }
}

void gc_collect(void)
{
    // WARNING: This gc_collect implementation doesn't try to get root
    // pointers from CPU registers, and thus may function incorrectly.
    void* dummy = NULL;
    
    gc_collect_start();
    gc_collect_root(&dummy, ((mp_uint_t)g_stack_top - (mp_uint_t)&dummy) / sizeof(mp_uint_t));
    gc_collect_end();
    gc_dump_info();
}

void nlr_jump_fail(void* val)
{
    while( 1 );
}

void DTPython_Init()
{
    static mp_obj_str_t mp_sys_dot_obj = {{&mp_type_str}, 0, 1, (const byte *)"."};
    int stack_dummy = 0;
    mp_obj_list_t* sys_path = NULL;
    
    g_stack_top = (char*)&stack_dummy;

    gc_init(g_heap, g_heap + sizeof(g_heap));
    
    
    sys_path = &MP_STATE_VM(mp_sys_path_obj);
    
    mp_obj_list_init(sys_path, 1);
    
    sys_path->items[0] = &mp_sys_dot_obj;
    
    mp_init();
}

void DTPython_RunFile(const char* file)
{
    if(file != NULL)
    {
        do_file(file);
    }
}

void DTPython_RunCode(const char* statement)
{
    if( statement != NULL )
    {
        do_str(statement, MP_PARSE_FILE_INPUT);
    }
}

void DTPython_Deinit()
{
    mp_deinit();
}
