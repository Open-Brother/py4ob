# Python for HOS  
  
#### 介绍
这个仓库是为了能够在鸿蒙设备上使用 Python 进行应用程序开发而创建。  
[0. 使用Python开发鸿蒙设备程序（0-初体验）](https://harmonyos.51cto.com/posts/1887)  
[1. 使用Python开发鸿蒙设备程序（1-GPIO外设控制）](https://harmonyos.51cto.com/posts/8428)  
[2. 使用Python开发鸿蒙设备程序（2-I2C应用实例）](https://harmonyos.51cto.com/posts/8594) 

#### 软件架构
这个仓库的 Baseline 是 [MicroPython v1.13](https://github.com/micropython/micropython/tree/v1.13)，在 MicroPython 的基础上进行了必要的剪裁以满足 OpenHarmony 上的应用开发需求。  
  
#### 编译说明
1.  编译环境：  
    1）OS - Ubuntu 16+  
    2）Make - 3.81+  
    3）Python - 3.8+  
2.  配置项路径：  
    1）打开源码根目录中的 Makefile  
    2）配置项目路径，如： PROPATH = /home/harmony/harmony/code/code-1.0
    3）配置编译器路径，如：CMPPATH = /home/harmony/gcc_riscv32/bin
3.  在源码根目录中执行 make  
  
#### 使用说明
1.  将编译得到的库文件 //build/libdtpython.a 拷贝到 //vendor/hisi/hi3861/hi3861/build/libs 目录下，如图：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1130/102742_bea41b9a_8048968.png "82c43e952c89d664c1513719385ef6b12ba1a7.png")  
2.  在设备应用中加载 Python 并执行代码  

```
static void* DTPython_Demo_Task(const char* arg)
{
    printf("[dt4sw] DTPython_Demo_Task()\n");

    PrepareScript("main.py", c_main_py);                 // 在设备上创建文件：main.py
    PrepareScript("test.py", c_test_py);                 // 在设备上创建文件：test.py
    PrepareScript("another.py", c_another_py);           // 在设备上创建文件：another.py

    DTPython_Init();                                     // 初始化Python环境

    DTPython_RunCode("print(\'Python Code Begin\')");    // 执行Python语句：print('Python Code Begin')
    
    DTPython_RunCode("s = \'Created by Delphi Tang\'");  // 执行Python语句：s = 'Created by Delphi Tang'

    DTPython_RunCode("print(s)");                        // 执行Python语句：print(s)

    DTPython_RunFile("main.py");                         // 执行Python文件：main.py

    DTPython_RunCode("print(\'Python Code End\')");      // 执行Python语句：print('Python Code End')

    DTPython_Deinit();                                   // 清理Python环境

    return (void*)arg;
}

```



