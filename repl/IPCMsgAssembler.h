/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

#ifndef IPCMSGASSEMBLER_H
#define IPCMSGASSEMBLER_H

#include <QObject>
#include <QQueue>
#include <QSharedPointer>
#include "IPCMessage.h"

class IPCMsgAssembler : public QObject
{
    QQueue<uchar> m_queue;
    IPCMessage* m_pMsg;

    void clear();
    QByteArray fetch(int n);
    IPCMessage* makePart();
    bool makeLast(IPCMessage& msg);
public:
    IPCMsgAssembler(QObject* parent = NULL);
    void prepare(const char* data, int len);
    QSharedPointer<IPCMessage> assemble(const char* data, int len);
    QSharedPointer<IPCMessage> assemble();
    void reset();
};

#endif // IPCMSGASSEMBLER_H
