/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

#ifndef REPLAPP_H
#define REPLAPP_H

#include <QObject>
#include <QMap>
#include <QDateTime>
#include <QTimer>
#include <QSerialPort>
#include "IPCClient.h"
#include "IPCEndPoint.h"
#include "IPCMsgHandler.h"

class ReplApp : public QObject, public IPCMsgHandler
{
    Q_OBJECT

    enum
    {
        DEVPORT = 8899,
        RXPORT = 9000
    };

    typedef void(ReplApp::*MsgHandler)(QAbstractSocket&, IPCMessage&);
    typedef void(ReplApp::*CmdHandler)(QString);

    struct CmdParam
    {
        CmdHandler handler;
        bool conn;

        CmdParam()
        {

        }

        CmdParam(CmdHandler h, bool c)
        {
            handler = h;
            conn = c;
        }
    };

    IPCClient m_client;
    IPCEndPoint m_endpoint;
    QMap<ushort, MsgHandler> m_msgHandlerMap;
    QMap<QString, CmdParam> m_cmdHandlerMap;
    QMap<QString, QDateTime> m_ipMap;
    QTimer m_timer;
    QSerialPort m_sp;

    void process(QString cmd);
    void doCode(QString code);
    void doCmd(QString cmd);

    void State_Handler(QAbstractSocket&, IPCMessage&);
    void Cmd_Handler(QAbstractSocket&, IPCMessage&);
    void Msg_Handler(QAbstractSocket&, IPCMessage&);
    void CMD_RUN_ACK_Handler(QAbstractSocket&, IPCMessage&);
    void CMD_CODE_ACK_Handler(QAbstractSocket&, IPCMessage&);
    void CMD_FILE_ACK_Handler(QAbstractSocket&, IPCMessage&);
    void STATE_CONN_Handler(QAbstractSocket&, IPCMessage&);
    void STATE_DSCN_Handler(QAbstractSocket&, IPCMessage&);
    void STATE_SIGNAL_Handler(QAbstractSocket&, IPCMessage&);
    void STATE_HOST_Handler(QAbstractSocket&, IPCMessage&);

    void Config_Wifi_Handler(QString, QString, QString);
    void Config_Mode_Handler(QString, QString);
    void Config_Handler(QString);
    void Query_Handler(QString);
    void Install_Handler(QString);
    void Run_Handler(QString);
    void Connect_Handler(QString);
    void Disconnect_Handler(QString);
    void Workspace_Handler(QString);
    void Exit_Handler(QString);

    void serialPortAction(QString port, QString atcmd, QString err);
    bool writeToDev(QString port, QString atcmd);
    void appendText(QString msg);
    void appendLine(int n = 1);

    ReplApp();

protected slots:
    void onReadFileProcess(ushort, ushort, const QByteArray&);
    void onCommand(QString cmd);
    void onTimeout();
    void onReadyRead();
public:
    static ReplApp& Instance();
    void handle(QAbstractSocket& sock, IPCMessage& msg);
    void run();
    ~ReplApp();
};

#endif // MSGHANDLER_H
