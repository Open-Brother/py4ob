/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

#ifndef SERIALMSGPARSER_H
#define SERIALMSGPARSER_H

#include <QObject>
#include <QQueue>

class SerialMsgParser : public QObject
{
    Q_OBJECT

protected:
    enum
    {
        STATUS_INIT,
        STATUS_RX,
        STATUS_FINISH
    };

    QString m_begin;
    QString m_end;
    QString m_cache;
    QString m_result;
    QQueue<QChar> m_queue;
    int m_pointer;
    int m_status;

    void doInit(QChar c);
    void doRx(QChar c);
    void doFinish(QChar c);
public:
    SerialMsgParser(QString begin, QString end, QObject* parent = NULL);

    void put(QString s);
    void put(QChar c);
    bool parse();
    void reset();
    QString result();
};

#endif // SERIALMSGPARSER_H
