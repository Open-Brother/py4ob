/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

#include "CmdLine.h"
#include <QSemaphore>
#include <iostream>

using namespace std;

static QSemaphore g_sem(1);

CmdLine& CmdLine::Instance()
{
    static CmdLine* pCmd = NULL;

    if( !pCmd )
    {
        pCmd = new CmdLine();
    }

    return *pCmd;
}

CmdLine::CmdLine()
{
    m_run = true;
}

void CmdLine::obtain()
{
    g_sem.acquire();
}

void CmdLine::giveup()
{
    if( !g_sem.available() )
    {
        g_sem.release();
    }
}

void CmdLine::print(QString s)
{
    cout << s.toStdString().c_str();
}

void CmdLine::println(int n)
{
    for(int i=0; i<n; i++)
    {
        cout << endl;
    }
}

void CmdLine::stop()
{
    m_run = false;
}

void CmdLine::run()
{
    char buf[128] = {0};

    cout << "Py4OH REPL v1.0.1     https://gitee.com/delphi-tang/python-for-hos" << endl;
    cout << "Author: Tang ZuoLin" << endl;
    cout << "WeChat: delphi_tang" << endl;
    cout << "E-Mail: delphi_tang@dt4sw.com" << endl;
    cout << endl;

    cout.flush();

    while( m_run )
    {
        QString cmd;

        obtain();

        cout << "D.T.Py4OH >>> ";

        cin.getline(buf, sizeof(buf) - 1);

        cmd = QString(buf).trimmed();

        giveup();

        if( cmd.length() )
        {
            emit command(cmd);
        }
    }
}


