/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

#include "SerialMsgParser.h"

SerialMsgParser::SerialMsgParser(QString begin, QString end, QObject *parent) : QObject(parent)
{
    m_begin = begin;
    m_end = end;

    reset();
}

void SerialMsgParser::put(QString s)
{
    for(int i=0; i<s.length(); i++)
    {
        put(s[i]);
    }
}

void SerialMsgParser::put(QChar c)
{
    m_queue.append(c);
}

void SerialMsgParser::doInit(QChar c)
{
    m_pointer = (m_begin[m_pointer] == c ) ? (m_pointer + 1) : 0;

    if( m_pointer == m_begin.length() )
    {
        m_pointer = 0;
        m_status = STATUS_RX;
    }
}

void SerialMsgParser::doRx(QChar c)
{
    while( 1 )
    {
        m_pointer = (m_end[m_pointer] == c) ? (m_pointer + 1) : 0;

        if( m_pointer == 0 )
        {
            if( m_cache.length() )
            {
                m_result.append(m_cache);
                m_cache = "";
            }
            else
            {
                m_result.append(c);
                break;
            }
        }
        else
        {
            m_cache.append(c);
            break;
        }
    }

    if( m_pointer == m_end.length() )
    {
        m_pointer = 0;
        m_status = STATUS_FINISH;
        m_cache = "";
    }
}

void SerialMsgParser::doFinish(QChar c)
{
    (void)c;
}

bool SerialMsgParser::parse()
{
    while( m_queue.size() )
    {
        QChar c = m_queue.front();

        switch(m_status)
        {
            case STATUS_INIT:
                doInit(c);
                break;
            case STATUS_RX:
                doRx(c);
                break;
            case STATUS_FINISH:
                doFinish(c);
                break;
        }

        m_queue.pop_front();
    }

    return (m_status == STATUS_FINISH);
}

void SerialMsgParser::reset()
{
    m_queue.clear();

    m_pointer = 0;
    m_cache = "";
    m_result = "";
    m_status = STATUS_INIT;
}

QString SerialMsgParser::result()
{
    return m_result;
}
