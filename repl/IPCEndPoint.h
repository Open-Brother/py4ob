/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

#ifndef IPCENDPOINT_H
#define IPCENDPOINT_H

#include <QObject>
#include <QUdpSocket>
#include "IPCMsgHandler.h"
#include "IPCMsgAssembler.h"

class IPCEndPoint : public QObject
{
    Q_OBJECT

    QUdpSocket m_endpoint;
    IPCMsgAssembler m_assembler;
    IPCMsgHandler* m_handler;
protected slots:
    void onDataReady();
    void onBytesWritten(qint64 bytes);
    void onStateChanged(QAbstractSocket::SocketState socketState);
public:
    IPCEndPoint(QObject* parent = NULL);
    bool bind(int port);
    qint64 send(const IPCMessage& message, QString ip, int port);
    qint64 available();
    void setHandler(IPCMsgHandler* handler);
    bool isValid();
    void close();
};

#endif // IPCENDPOINT_H
