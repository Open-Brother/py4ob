
QT += core network serialport

CONFIG += c++11 console

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp \
    IPCMessage.cpp \
    IPCMsgAssembler.cpp \
    ReadFileThread.cpp \
    IPCClient.cpp \
    ReplApp.cpp \
    CmdLine.cpp \
    IPCEndPoint.cpp \
    SerialMsgParser.cpp

HEADERS += \
    IPCMessage.h \
    IPCMsgAssembler.h \
    IPCMsgHandler.h \
    IPCEvent.h \
    ReadFileThread.h \
    IPCClient.h \
    ReplApp.h \
    CmdLine.h \
    IPCEndPoint.h \
    SerialMsgParser.h
