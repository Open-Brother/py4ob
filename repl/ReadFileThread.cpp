/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

#include "ReadFileThread.h"
#include <QFile>

#define BUFSIZE  (5 * 1024)

ReadFileThread::ReadFileThread(QString file, QObject* parent) : QThread(parent)
{
    m_file = file;
}

ReadFileThread* ReadFileThread::Create(QString file, QObject* parent)
{
    return new ReadFileThread(file, parent);
}

void ReadFileThread::run()
{
    QFile f(m_file);

    if( f.open(QIODevice::ReadOnly) )
    {
        int count = f.bytesAvailable() / BUFSIZE;
        int more = f.bytesAvailable() % BUFSIZE;
        int total = count + !!more;

        emit process(0, total, m_file.toStdString().c_str());

        for(int i=1; i<=count; i++)
        {
            emit process(i, total, f.read(BUFSIZE));
            msleep(50);
        }

        if( more )
        {
            emit process(total, total, f.read(more));
        }
    }
    else
    {
        emit process(0, 0, m_file.toStdString().c_str());
    }

    deleteLater();
}

ReadFileThread::~ReadFileThread()
{

}
