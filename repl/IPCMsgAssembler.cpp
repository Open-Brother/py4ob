/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
#include "IPCMsgAssembler.h"

IPCMsgAssembler::IPCMsgAssembler(QObject* parent) : QObject(parent)
{
    m_pMsg = NULL;
}

void IPCMsgAssembler::clear()
{
    if( m_pMsg != NULL )
    {
        delete m_pMsg;
        m_pMsg = NULL;
    }
}

QByteArray IPCMsgAssembler::fetch(int n)
{
    QByteArray ret;

    for(int i=0; i<n; i++)
    {
        ret.append(m_queue.dequeue());
    }

    return ret;
}

void IPCMsgAssembler::prepare(const char* data, int len)
{
    if( data != NULL )
    {
        for(int i=0; i<len; i++)
        {
            m_queue.enqueue(data[i]);
        }
    }
}

QSharedPointer<IPCMessage> IPCMsgAssembler::assemble()
{
    IPCMessage* ret = NULL;

    if( m_pMsg == NULL )
    {
        m_pMsg = makePart();
    }

    if( (m_pMsg != NULL) && makeLast(*m_pMsg) )
    {
        ret = m_pMsg;
        m_pMsg = NULL;
    }

    return QSharedPointer<IPCMessage>(ret);
}

QSharedPointer<IPCMessage> IPCMsgAssembler::assemble(const char* data, int len)
{
    prepare(data, len);

    return assemble();
}

IPCMessage* IPCMsgAssembler::makePart()
{
    IPCMessage* ret = NULL;

    if( m_queue.length() >= IPCMessage::MINSIZE )
    {
        ret = new IPCMessage();

        IPCMessage::TryMakePart(*ret, fetch(IPCMessage::MINSIZE));
    }

    return ret;
}

bool IPCMsgAssembler::makeLast(IPCMessage& msg)
{
    bool ret = (m_queue.length() >= msg.length());

    if( ret )
    {
        msg.setPayload(fetch(msg.length()));
    }

    return ret;
}

void IPCMsgAssembler::reset()
{
    clear();
    m_queue.clear();
}
