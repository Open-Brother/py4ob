/*

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
#include "IPCMessage.h"


IPCMessage::IPCMessage(QObject* parent) : QObject(parent)
{
    (void)parent;

    m_header.type = 0;
    m_header.cmd = 0;
    m_header.index = 0;
    m_header.total = 0;
    m_header.length = 0;
}

IPCMessage::IPCMessage(ushort type, ushort cmd, ushort index, ushort total, const char* payload, int length, QObject* parent) : QObject(parent)
{
    (void)parent;

    m_header.type = type;
    m_header.cmd = cmd;
    m_header.index = index;
    m_header.total = total;
    m_header.length = 0;

    setPayload(payload, length);
}

IPCMessage::IPCMessage(ushort type, ushort cmd, ushort index, ushort total, const QByteArray& payload, QObject* parent) : QObject(parent)
{
    (void)parent;

    m_header.type = type;
    m_header.cmd = cmd;
    m_header.index = index;
    m_header.total = total;
    m_header.length = 0;

    setPayload(payload);
}

ushort IPCMessage::type() const
{
    return m_header.type;
}

ushort IPCMessage::cmd() const
{
    return m_header.cmd;
}

ushort IPCMessage::index() const
{
    return m_header.index;
}

ushort IPCMessage::total() const
{
    return m_header.total;
}

const QByteArray& IPCMessage::payload() const
{
    return m_payload;
}

bool IPCMessage::valid() const
{
    return (m_header.length == (unsigned int)m_payload.length());
}

int IPCMessage::length() const
{
    return m_header.length;
}

int IPCMessage::size() const
{
    return MINSIZE + length();
}

bool IPCMessage::setPayload(const char* payload, int length)
{
    bool ret = (payload != NULL);

    if( ret )
    {
        m_header.length = length;
        m_payload.clear();
        m_payload.append(payload, length);
    }

    return ret;
}

bool IPCMessage::setPayload(const QByteArray& payload)
{
    m_header.length = payload.length();
    m_payload.clear();
    m_payload.append(payload);

    return true;
}

QByteArray IPCMessage::serialize() const
{
    QByteArray ret;

    ret.append((char)((m_header.type >> 8) & 0xFF));
    ret.append((char)(m_header.type & 0xFF));
    ret.append((char)((m_header.cmd >> 8) & 0xFF));
    ret.append((char)(m_header.cmd & 0xFF));
    ret.append((char)((m_header.index >> 8) & 0xFF));
    ret.append((char)(m_header.index & 0xFF));
    ret.append((char)((m_header.total >> 8) & 0xFF));
    ret.append((char)(m_header.total & 0xFF));

    ret.append((char)((m_header.length >> 24) & 0xFF));
    ret.append((char)((m_header.length >> 16) & 0xFF));
    ret.append((char)((m_header.length >> 8) & 0xFF));
    ret.append((char)(m_header.length & 0xFF));

    ret.append(m_payload);

    return ret;
}

bool IPCMessage::unserialize(QByteArray ba)
{
    return TryMake(*this, ba);
}

bool IPCMessage::TryMake(IPCMessage& msg, const QByteArray& ba)
{
    bool ret = false;
    MsgHeader header;

    if ( (ret = (TryMake(header, ba)) && (header.length + MINSIZE <= (unsigned int)ba.length())) )
    {
        msg.m_header = header;

        msg.m_payload.clear();

        msg.m_payload.append(ba.data() + MINSIZE, header.length);
    }

    return ret;
}

bool IPCMessage::TryMakePart(IPCMessage& msg, const QByteArray& ba)
{
    bool ret = TryMake(msg.m_header, ba);

    if( ret )
    {
        msg.m_payload.clear();
    }

    return ret;
}

bool IPCMessage::TryMake(MsgHeader& header, const QByteArray& ba)
{
    bool ret = (ba.length() >= MINSIZE);

    if( ret )
    {
        header.type = (ushort)(((uchar)ba[0] << 8) | (uchar)ba[1]);
        header.cmd = (ushort)(((uchar)ba[2] << 8) | (uchar)ba[3]);
        header.index = (ushort)(((uchar)ba[4] << 8) | (uchar)ba[5]);
        header.total = (ushort)(((uchar)ba[6] << 8) | (uchar)ba[7]);
        header.length = (((uchar)ba[8] << 24) | ((uchar)ba[9] << 16) | ((uchar)ba[10] << 8) | (uchar)ba[11]);
    }

    return ret;
}
